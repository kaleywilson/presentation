import cv2
import yolo
cam = cv2.VideoCapture(0)
if not cam.isOpened():
    print("Camera is not opened")
else:
    while True:
        ret_val, img = cam.read()
        img = yolo.label_image(r'C:\Users\Study\Desktop\Presentation\yolo-coco', img, 0.3, 0.3)
        cv2.imshow("My Webcam", img)
        if cv2.waitKey(1) == 27:
            break
    cv2.destroyAllWindows()